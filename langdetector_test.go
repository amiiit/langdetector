package langdetector_test

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/ajk/langdetector"
)

var _ = Describe("langdetector", func() {

	handler := langdetector.LanguageDetector{}.ServeHTTP
	w := &httptest.ResponseRecorder{}

	BeforeEach(func() {
		w = httptest.NewRecorder()
	})

	It(`For POST request show postVar value and language and method`, func() {

		data := url.Values{}
		data.Set("postVar", "myPostVar")

		req, err := http.NewRequest("POST", "/", bytes.NewBufferString(data.Encode()))

		req.Header["Content-Type"] = []string{"application/x-www-form-urlencoded"}
		req.Header["Accept-Language"] = []string{"de-DE,de;q=0.8,en;q=0.6,es;q=0.4,he;q=0.2"}
		Ω(err).Should(BeNil())
		handler(w, req)
		Ω(w.Code).Should(Equal(http.StatusOK))

		bodyString := fmt.Sprintf("%s", w.Body)

		Ω(bodyString).To(ContainSubstring("Your language is: de-DE"))
		Ω(bodyString).To(ContainSubstring("You sent a: POST request"))
		Ω(bodyString).To(ContainSubstring("Your POST variable value: myPostVar"))
	})

	It(`For a successful GET request, returns a properly
	encoded HTML with browser language and request type`, func() {
		req, err := http.NewRequest("GET", "/", nil)
		req.Header["Accept-Language"] = []string{"en-US,en;q=0.8,de;q=0.6,es;q=0.4,he;q=0.2"}
		Ω(err).Should(BeNil())
		handler(w, req)
		Ω(w.Code).Should(Equal(http.StatusOK))

		bodyString := fmt.Sprintf("%s", w.Body)

		Ω(bodyString).To(ContainSubstring("Your language is: en-US"))
		Ω(bodyString).To(ContainSubstring("You sent a: GET request"))

	})

	It(`For any POST request, checks for the existence of a parameter named "postVar"
	 and returns a descriptive HTTP error code if it does not exist.
	Also, return a properly encoded HTML document with a simple error message.`, func() {

		req, err := http.NewRequest("POST", "/", nil)
		Ω(err).Should(BeNil())

		handler(w, req)

		Ω(w.Code).Should(Equal(http.StatusNotAcceptable))

		bodyString := fmt.Sprintf("%s", w.Body)
		Ω(bodyString).To(ContainSubstring("<html"))
		Ω(bodyString).To(ContainSubstring("</html>"))

	})

	It(`Responds only to GET and POST requests,
	and returns the proper HTTP error code if a
	different HTTP verb is requested.`, func() {

		req, err := http.NewRequest("PATCH", "/", nil)
		Ω(err).Should(BeNil())

		handler(w, req)

		Ω(w.Code).Should(Equal(http.StatusMethodNotAllowed))
	})

	It(`Only accepts requests on the index URL: "/",
	and returns the proper HTTP error code if a
	different URL is requested`, func() {
		req, err := http.NewRequest("GET", "/la", nil)
		Ω(err).Should(BeNil())

		handler(w, req)

		Ω(w.Code).Should(Equal(404))
	})

})
