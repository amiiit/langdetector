Langdetector
============

This is a little learning project. Use this as a service to help users detect their
own browsers' `Accepted-Language` and see the `POST` parameter named `postVar` in a
readable HTML format

Test and run
============

If you don't have a GitLab account:

```
$ git clone https://gitlab.com/ajk/langdetector.git
```

Otherwise clone over SSH:

```
$ git clone git@gitlab.com:ajk/langdetector.git
```

Go get:

```
$ go get -v ./...
# Test suite dependencies
$ go get github.com/onsi/ginkgo
$ go get github.com/onsi/gomega
```

Run tests:

```
$ go test
```

Run to have the project listening on localhost:8000

```
$ go run cmd/langdetector/main.go 
langdetector: will listen on port 8000
```
