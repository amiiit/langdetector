package langdetector_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestLangdetector(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Langdetector Suite")

}
