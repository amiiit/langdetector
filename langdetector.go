package langdetector

import (
	"fmt"
	"html/template"
	"net/http"
	"strings"

	"golang.org/x/text/language"
)

type LanguageDetector struct{}

func postHandler(w http.ResponseWriter, r *http.Request) {
	postVar := r.FormValue("postVar")
	if postVar == "" {
		w.WriteHeader(http.StatusNotAcceptable)
		t, _ := template.ParseFiles("postvar-missing.html")
		t.Execute(w, nil)
		return
	}
	legalRequestHandler(w, r)
}

type templateData struct {
	Method   string
	PostVar  string
	IsPost   bool
	Language string
}

func legalRequestHandler(w http.ResponseWriter, r *http.Request) {

	data := templateData{
		Method:  r.Method,
		PostVar: r.FormValue("postVar"),
		IsPost:  r.Method == "POST",
	}

	languageHeader := r.Header["Accept-Language"]
	if languageHeader != nil {
		tag, _, _ := language.ParseAcceptLanguage(languageHeader[0])
		data.Language = tag[0].String()
	}

	t, _ := template.ParseFiles("response.html")

	t.Execute(w, data)
}

func getHandler(w http.ResponseWriter, r *http.Request) {
	legalRequestHandler(w, r)
}

func badMethodHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusMethodNotAllowed)
	fmt.Fprint(w, "Only GET and POST are supported")
}

func (ld LanguageDetector) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	if r.Method != "GET" && r.Method != "POST" {
		badMethodHandler(w, r)
		return
	}

	if strings.Split(r.URL.Path, "&")[0] != "/" {
		http.NotFound(w, r)
		return
	}

	switch r.Method {
	case "GET":
		getHandler(w, r)
		return
	case "POST":
		postHandler(w, r)
		return
	default:
		panic(fmt.Sprintf("Missing handler for method %s", r.Method))
	}

}
