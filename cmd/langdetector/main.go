package main

import (
	"log"
	"net/http"

	"fmt"

	"gitlab.com/ajk/langdetector"
)

func main() {
	http.HandleFunc("/", langdetector.LanguageDetector{}.ServeHTTP)
	fmt.Println("langdetector: will listen on port 8000")
	log.Fatal(http.ListenAndServe(":8000", nil))
}
